/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeinheritance;

/**
 *
 * @author EAK
 */
public class Rectangle extends Shape {

    protected double w, h;

    public Rectangle(double w, double h) {
        System.out.println("Rectangle created");
        this.w = w;
        this.h = h;
    }

    @Override
    public double calArea() {
        return w * h;
    }

    @Override
    public void print() {
        System.out.println("Rectangle: width = " + this.w + " height = " + this.h + " Area = " + calArea());
    }
}
