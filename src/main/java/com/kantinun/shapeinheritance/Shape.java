/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeinheritance;

/**
 *
 * @author EAK
 */
public class Shape {
    public Shape(){
        System.out.println("Shape created");
    }
    
    public double calArea(){
        return 0.0;
    }
    
    public void print(){
        System.out.println("Shape area = " + calArea());
    }
}
